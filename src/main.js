import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router'
import './assets/css/global.css'
import './plugins/element.js'
import './assets/fonts/iconfont.css'
import ZkTable from 'vue-table-with-tree-grid'
import elCascaderMulti from 'el-cascader-multi'
// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor'
// 富文本 styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.use(VueQuillEditor)

Vue.component('tree-table', ZkTable)
Vue.config.productionTip = false
// 级联选择器
Vue.use(elCascaderMulti)
// 配置axios
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/private/v1/'
axios.interceptors.request.use(config => {
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})
Vue.prototype.$http = axios
Vue.filter('dateFormat', function (olddate) {
  const data = new Date(olddate)
  const y = data.getFullYear()
  const m = (data.getMonth() + 1 + '').padStart(2, '0')
  const d = (data.getDate() + '').padStart(2, '0')
  const hh = (data.getHours() + '').padStart(2, '0')
  const mm = (data.getMinutes() + '').padStart(2, '0')
  const ss = (data.getSeconds() + '').padStart(2, '0')
  return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
