import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/login'
import Home from '../components/home'
import Welecom from '../components/welcom'
import Users from '../components/user/users'
import Rights from '../components/power/rights'
import Roles from '../components/power/roles'
import Cart from '../components/goods/Cart'
import Params from '../components/goods/Params'
import List from '../components/goods/List'
import AddGoods from '../components/goods/AddGoods.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: '/welecom',
        component: Welecom
      },
      {
        path: '/users',
        component: Users
      },
      {
        path: '/rights',
        component: Rights
      },
      {
        path: '/roles',
        component: Roles
      },
      {
        path: '/categories',
        component: Cart
      },
      {
        path: '/params',
        component: Params
      },
      {
        path: '/goods',
        component: List
      },
      {
        path: '/addGoods',
        component: AddGoods
      }
    ],
    redirect: '/welecom'
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  // to要到那里去，from是从那里来  next（）放行 next(路径)跳转到某个页面
  if (to.path === '/login') return next() // 如果是登陆页面就放行
  let token = window.sessionStorage.getItem('token')
  if (!token) return next('/login') // 如果没有拿到token就跳转到登陆页面
  next() // 有的放就放行
})

// 去除重复点击上次路由报错
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}
export default router
